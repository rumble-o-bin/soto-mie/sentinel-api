package server

import (
	"log"
	"net/http"
	"sentinel-api/config"
	"sentinel-api/pkg/metrics"
	"sentinel-api/pkg/router"

	"github.com/go-redis/redis/v8"
)

func StartServer(cfg config.ServerConfigurations, client *redis.Client) {
	// Prometheus metrics setup
	metrics := metrics.NewMetrics()

	// Create router
	httpHandler := router.NewRouter(cfg, client, metrics)

	// Metrics HTTP handler
	metricsMux := http.NewServeMux()
	metricsMux.Handle("/metrics", metrics.Handler())

	// Start API server with the router
	go func() {
		log.Printf("API server listening on port %s", cfg.HttpPort)
		log.Fatal(http.ListenAndServe(":"+cfg.HttpPort, httpHandler))
	}()

	// Start Metrics server
	go func() {
		log.Printf("Metrics server listening on port %s", cfg.MetricsPort)
		log.Fatal(http.ListenAndServe(":"+cfg.MetricsPort, metricsMux))
	}()

	// Block forever
	select {}
}
