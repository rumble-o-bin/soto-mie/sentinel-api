package middleware

import (
	"context"
	"net/http"

	"github.com/go-redis/redis/v8"
)

func RedisMiddleware(client *redis.Client, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := client.Ping(context.Background()).Err()
		if err != nil {
			http.Error(w, "Redis is not available", http.StatusInternalServerError)
			return
		}
		next.ServeHTTP(w, r)
	})
}
