package router

import (
	"context"
	"encoding/json"
	"math/rand"
	"net/http"
	"sentinel-api/config"
	"sentinel-api/pkg/metrics"
	"sentinel-api/pkg/middleware"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/prometheus/client_golang/prometheus"
)

func NewRouter(cfg config.ServerConfigurations, client *redis.Client, metrics *metrics.Metrics) http.Handler {
	router := http.NewServeMux()

	router.HandleFunc("/api/message", func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		delay := time.Duration(rand.Intn(2000)) * time.Millisecond
		time.Sleep(delay)

		var statusCode int
		if delay > time.Second {
			statusCode = http.StatusInternalServerError
			w.WriteHeader(statusCode)
			w.Write([]byte("Internal server error"))
		} else {
			statusCode = http.StatusOK
			w.WriteHeader(statusCode)
			w.Write([]byte("Hello, World!"))
		}

		labels := prometheus.Labels{
			"path":        "api/message",
			"method":      r.Method,
			"status_code": strconv.Itoa(statusCode),
		}

		metrics.Requests.With(labels).Inc()
		metrics.RequestDurations.With(labels).Observe(time.Since(start).Seconds())
		//
		// Just return hello world in here
		//
		// w.WriteHeader(http.StatusOK)
		// w.Write([]byte("Hello, World!"))

		// // Record Prometheus metrics
		// labels := prometheus.Labels{
		// 	"path":        "/api/message",
		// 	"method":      r.Method,
		// 	"status_code": strconv.Itoa(http.StatusOK),
		// }
		// metrics.Requests.With(labels).Inc()
		// metrics.RequestDurations.With(labels).Observe(0)
	})

	router.HandleFunc("/api/hello", func(w http.ResponseWriter, r *http.Request) {
		randomSource := rand.NewSource(time.Now().UnixNano())
		randomGenerator := rand.New(randomSource)
		key := "random_key_" + strconv.Itoa(randomGenerator.Intn(1000000))
		value := "random_value_" + strconv.Itoa(randomGenerator.Intn(1000000))

		ctx := context.Background()

		// Store key-value pair in Redis
		err := client.Set(ctx, key, value, 0).Err()
		if err != nil {
			http.Error(w, "Failed to store data in Redis", http.StatusInternalServerError)
			return
		}

		// Respond with the key-value pair
		response := map[string]string{"key": key, "value": value}
		jsonResponse, err := json.Marshal(response)
		if err != nil {
			http.Error(w, "Failed to marshal JSON response", http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(jsonResponse)

		// Record Prometheus metrics
		labels := prometheus.Labels{
			"path":        "/api/hello",
			"method":      r.Method,
			"status_code": strconv.Itoa(http.StatusOK),
		}
		metrics.Requests.With(labels).Inc()
		metrics.RequestDurations.With(labels).Observe(0)
	})

	router.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		// Example ping response
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("pong"))
	})

	return middleware.RedisMiddleware(client, router)
}
