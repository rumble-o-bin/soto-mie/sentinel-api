package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Metrics struct {
	Requests         *prometheus.CounterVec
	RequestDurations *prometheus.HistogramVec
}

// NewMetrics creates and registers Prometheus metrics.
func NewMetrics() *Metrics {
	requests := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Number of get requests.",
		},
		[]string{"path", "method", "status_code"},
	)
	prometheus.MustRegister(requests)

	requestDurations := prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_request_duration_seconds",
			Help:    "Histogram of request durations in seconds.",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"path", "method", "status_code"},
	)
	prometheus.MustRegister(requestDurations)

	return &Metrics{
		Requests:         requests,
		RequestDurations: requestDurations,
	}
}

func (m *Metrics) Handler() http.Handler {
	return promhttp.Handler()
}
