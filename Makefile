CLUSTER_NAME="kind-demow"
CLUSTER_VERSION="kindest/node:v1.27.11"

.DEFAULT_GOAL := help
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

build: ## build binary
	@CGO_ENABLED=0 go build -gcflags=all="-l -B -C" -ldflags="-s -w -v" -o dist/sentinel-api cmd/main.go

build-container: ## build container
	@docker build -t riskiwah/sentinel-api:native .

run-container: ## run local container
	@docker run --rm -p 8080:8080 -p 8081:8081 -v $(CURDIR)/config/config.yaml:/cmd/sentinel-api/config/config.yaml riskiwah/sentinel-api:native

run-dev: ## running dev mode
	@go run cmd/main.go

#### Cluster and Deployment ####
start-cluster: ## start kind cluster
	@kind create cluster --name $(CLUSTER_NAME) --config deploy/cluster/kind-config.yaml --image $(CLUSTER_VERSION)
	@sleep 1
	@kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.14.5/config/manifests/metallb-native.yaml
	@sleep 30
	@kubectl apply -k deploy/cluster/

deploy-infra: ## deploy infra
	@bash deploy/charts/kube-prometheus-stack/crds.sh
	@sleep 2
	@HELM_DIFF_THREE_WAY_MERGE=true helmfile apply --color -f deploy/charts/helmfile.yaml

deploy-app: ## deploy app
	@kubectl apply -k deploy/app

delete-cluster: ## Delete cluster kind
	@kind delete cluster --name $(CLUSTER_NAME)
