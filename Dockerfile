ARG ALPINE_VERSION=3.19
ARG GOLANG_VERSION=1.21.9
FROM golang:${GOLANG_VERSION}-alpine as builder
RUN apk --no-cache add git make
WORKDIR /sentinel-api
ADD . /sentinel-api
RUN go mod download \
    && make build

FROM alpine:${ALPINE_VERSION} as runtime
RUN apk update \
    && apk add ca-certificates git tzdata tini \
    && rm -rf /var/cache/apk/*
ENV TZ=Asia/Jakarta
WORKDIR /cmd/sentinel-api
# do stpid things @_@
RUN mkdir -p config
COPY --from=builder /sentinel-api/dist/sentinel-api .

ENTRYPOINT ["/sbin/tini", "--"]
CMD [ "./sentinel-api" ]
