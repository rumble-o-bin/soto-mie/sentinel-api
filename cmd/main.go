package main

import (
	"log"
	"sentinel-api/config"
	"sentinel-api/pkg/server"

	"github.com/go-redis/redis/v8"
)

func main() {
	cfg, err := config.LoadConfig("./config")
	if err != nil {
		log.Fatalf("Failed to load configuration: %v", err)
	}

	client := redis.NewFailoverClient(&redis.FailoverOptions{
		MasterName:    cfg.Redis.RedisMaster,
		SentinelAddrs: cfg.Redis.SentinelAddrs,
	})

	server.StartServer(cfg.Server, client)
}
