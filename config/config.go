package config

import "github.com/spf13/viper"

type Configuration struct {
	Server ServerConfigurations
	Redis  RedisConfigurations
}

type ServerConfigurations struct {
	HttpPort    string `mapstructure:"http_port"`
	MetricsPort string `mapstructure:"metrics_port"`
}

type RedisConfigurations struct {
	RedisMaster   string   `mapstructure:"redis_master"`
	SentinelAddrs []string `mapstructure:"sentinel_address"`
}

func LoadConfig(path string) (Configuration, error) {
	viper.AddConfigPath(path)
	var cfg Configuration

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		return cfg, err
	}

	err = viper.Unmarshal(&cfg)
	if err != nil {
		return cfg, err
	}

	return cfg, nil
}
